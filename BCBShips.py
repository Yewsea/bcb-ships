import requests as rq
from bs4 import BeautifulSoup
import re
from collections import defaultdict

req = rq.get('https://www.bittersweetcandybowl.com/candybooru/tags/popularity')
results = req.content
reqsoup = BeautifulSoup(results, "html.parser")
# print(reqsoup.prettify())


def checkTerms(link_text):
    discard_terms = (
        "Artist", "Norix", "Extras", "excellent", "pixel_art", "expressionmeme", "sexy", "context", "sexualit", "next"
    )
    for term in discard_terms:
        if term.lower() in link_text:
            return True
    return False


def checkNames(shipToCheck):
    names = ("max", "roxy", "felix", "sex_ed_teacher", "kaxbe", "alexa")
    newShip = shipToCheck
    for name in names:
        if name in newShip:
            nameDup = name.replace('x', 'c', 1)
            newShip = shipToCheck.replace(name, nameDup)
            shipToCheck = newShip

    removeNumbers = shipToCheck.split("\xa0")
    shipNames = removeNumbers[0].split('x')
    if len(shipNames) == 1:
        return False
    defineNums = int(removeNumbers[-1][1:-1])

    return shipNames, defineNums


def correctName(name):
    name = name.replace('_', " ")
    names = ("mac", "rocy", "felic", "sec ed teacher", "macwell")
    if name not in names:
        name = name.title()
    else:
        repName = name.replace('c', 'x')
        name = repName.title()
    name = name.replace("’S", "'s")  # Stupid fucking tag
    return name.replace("'S", "'s")


def addToShipTotals(deets, dicts):
    characters = list(set(deets[0]))
    number = deets[1]

    for character in characters:
        character = correctName(character)
        dicts[character] += number


def addToAllShips(deets, dicts):
    characters = list(set(deets[0]))
    number = deets[1]

    if len(characters) == 1:  # self-love
        innerDicts = dicts[characters[0]]
        innerDicts[characters[0]] += number
    else:
        for character in characters:
            character = correctName(character)
            innerDicts = dicts[character]
            for partner in characters:
                partner = correctName(partner)
                if character == partner:
                    continue
                innerDicts[partner] += number


def checkTag(name, tagToCheck):
    tagString = name + "\xa0" + '('
    if tagString not in tagToCheck:
        return False
    if name[:3] != tagToCheck[:3]:
        return False

    removeNumbers = tagToCheck.split("\xa0")
    numbers = int(removeNumbers[-1][1:-1])
    return name, numbers

characterAppearences = defaultdict(int)
allShips = defaultdict(lambda: defaultdict(int))
totalShipValues = defaultdict(int)

find_links = reqsoup.body.findAll("a")
for link in find_links:
    link_text = link.text.lower()
    if checkTerms(link_text):
        continue
    if 'x' in link_text:
        shipDetails = checkNames(link_text)
        if shipDetails:
            # print(shipDetails)
            addToShipTotals(shipDetails, totalShipValues)
            addToAllShips(shipDetails, allShips)

print("Total Values")
print(len(totalShipValues.keys()), " characters have ships on the borru")
print(len([x for x in totalShipValues.keys() if totalShipValues[x] >= 5]), " characters have at least 5 ship pics on the borru")
for character in sorted(totalShipValues, key=totalShipValues.get, reverse=True):
    print(character, totalShipValues[character])

print("")
print("")
print("")
for character in sorted(totalShipValues, key=totalShipValues.get, reverse=True):
    if totalShipValues[character] < 5:
        continue
    characterDict = allShips[character]
    print(character)
    for partner in sorted(characterDict, key=characterDict.get, reverse=True):
        print(character, " x ", partner, characterDict[partner])
    print("")

for character in totalShipValues.keys():
    for link in find_links:
        link_text = link.text.lower()
        if character.lower() in link_text:
            tagDetails = checkTag(character.lower(), link_text)
            if tagDetails and tagDetails[1] != 1:
                character = correctName(tagDetails[0])
                print(character)
                print(totalShipValues[character], '/', tagDetails[1])
                percentage = int(totalShipValues[character] / tagDetails[1] * 10000)/100
                print(percentage, '%')
